#!/usr/bin/env python3
from {{cookiecutter.clean_name}}.scripts import start


if __name__ == "__main__":
    start()
