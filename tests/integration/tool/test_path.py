import pathlib
import tempfile


def test_copy(hub):
    with tempfile.TemporaryDirectory(prefix="pop-create-tests-", suffix="-src") as src:
        with tempfile.TemporaryDirectory(
            prefix="pop-create-tests-", suffix="-dst"
        ) as dst:
            srcdir = pathlib.Path(src)
            dstdir = pathlib.Path(dst)

            (srcdir / "new.txt").touch()
            src_exist_file = srcdir / "exist.txt"
            src_exist_file.write_text("overwritten")
            dst_exist_file = dstdir / "exist.txt"
            dst_exist_file.write_text("skipped")

            hub.tool.path.copy(srcdir, dstdir)
            actual_contents = dst_exist_file.read_text()
            assert actual_contents == "skipped", actual_contents


def test_copytree(hub):
    with tempfile.TemporaryDirectory(prefix="pop-create-tests-", suffix="-src") as src:
        with tempfile.TemporaryDirectory(
            prefix="pop-create-tests-", suffix="-dst"
        ) as dst:
            srcdir = pathlib.Path(src)
            dstdir = pathlib.Path(dst)

            hub.tool.path.copytree(srcdir, dstdir, hub.tool.path.copy)


def test_touch(hub):
    with tempfile.TemporaryDirectory(prefix="pop-create-tests-", suffix="-src") as src:
        new_path = pathlib.Path(src) / "foo" / "bar" / "baz.py"
        assert not new_path.exists()
        hub.tool.path.touch(new_path)
        assert new_path.exists()


def test_mkdir(hub):
    with tempfile.TemporaryDirectory(prefix="pop-create-tests-", suffix="-src") as src:
        new_path = pathlib.Path(src) / "foo" / "bar" / "baz"
        assert not new_path.exists()
        hub.tool.path.mkdir(new_path)
        assert new_path.exists()
